package com.greta94.golfs.models;

import java.util.ArrayList;
import java.util.Objects;

//Todo Javadoc
public class Courses {
    private int id;
    private String name;
    private ArrayList<Hole> holes;

    public Courses(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Hole> getHoles() {
        return holes;
    }

    public void setHoles(ArrayList<Hole> holes) {
        this.holes = holes;
    }

    @Override
    public String toString() {
        return "Parcours{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", holes=" + holes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Courses courses = (Courses) o;
        return id == courses.id && Objects.equals(name, courses.name) && Objects.equals(holes, courses.holes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, holes);
    }

}
