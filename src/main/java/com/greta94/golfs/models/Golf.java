package com.greta94.golfs.models;

import java.util.ArrayList;
import java.util.Objects;

//Todo Javadoc
public class Golf {
    private int id;
    private String name;
    private ArrayList<Courses> courses;

    public Golf(int id, String name, ArrayList<Courses> courses) {
        this.id = id;
        this.name = name;
        this.courses = courses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Courses> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Courses> courses) {
        this.courses = courses;
    }

    @Override
    public String toString() {
        return "Golf{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parcours=" + courses +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Golf golf = (Golf) o;
        return id == golf.id && Objects.equals(name, golf.name) && Objects.equals(courses, golf.courses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, courses);
    }
}
