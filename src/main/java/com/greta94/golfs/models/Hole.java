package com.greta94.golfs.models;

import java.util.Objects;

/**
 * Classe qui représente un trou
 *
 * @author Timothée
 * @version 1.0
 */
public class Hole {

    /**
     * Cet attribut clé primaire d'un trou en entier
     */
    private int id;
    /**
     * Cet attribut est le nom d'un trou en chaine
     */
    private String name;
    /**
     * Cet attribut est le numero du trou dans le parcours  en entier (de 1 à 18)
     */
    private int num;
    /**
     * Cet attribut est la difficulté d'un trou en entier (3 à 5)
     */
    private int par;
    /**
     * Cet attribut corespond au temps de marche (en entier ?)
     */
    private int walkTime;

    /**
     * Constructeur de la classe Hole (trou)
     * @param id la clé primaire d'un trou en entier
     * @param name le nom d'un trou en chaine
     * @param num le numero du trou dans le parcours  en entier (de 1 à 18)
     * @param par la difficulté d'un trou en entier (3 à 5)
     * @param walkTime le temps de marche (en entier ?)
     */
    public Hole(int id, String name, int num, int par, int walkTime) {
        this.id = id;
        this.name = name;
        this.num = num;
        this.par = par;
        this.walkTime = walkTime;
    }

    /**
     * Accesseur de l'attribut id d'un trou
     * @return la clé primaire d'un trou en entier
     */
    public int getId() {
        return id;
    }

    /**
     * Mutateur de l'attribut id d'un trou
     * @param id la nouvelle clé primaire d'un trou en entier
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Acceseur de l'attribut nom d'un trou
     * @return le nom d'un trou en chaine
     */
    public String getName() {
        return name;
    }

    /**
     * Mutateur de l'attribut nom d'un trou
     * @param name le nouveau nom du trou en chaine
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Acceseur de l'attribut num d'un trou
     * @return le numero du trou dans le parcours  en entier (de 1 à 18)
     */
    public int getNum() {
        return num;
    }

    /**
     * Mutateur de l'attribut num d'un trou
     * @param num la nouvelle position du trou dans le parcours
     */
    public void setNum(int num) {
        this.num = num;
    }

    /**
     * Acceseur de l'attribut par d'un trou
     * @return la difficulté (le par) d'un trou
     */
    public int getPar() {
        return par;
    }

    /**
     * Mutateur de l'attribut par d'un trou
     * @param par la nouvelle dificulté (par) d'un trou
     */
    public void setPar(int par) {
        this.par = par;
    }

    /**
     * Acceseur de l'attribut temps de marche d'un trou
     * @return le temps de marche
     */
    public int getWalkTime() {
        return walkTime;
    }

    /**
     * Mutateur de l'attribut temps de marche d'un trou
     * @param walkTime le nouveau temps de marche
     */
    public void setWalkTime(int walkTime) {
        this.walkTime = walkTime;
    }

    /**
     * Méthode qui permet de comparer 2 trous
     * @param o un objet que l'on compare à un trou
     * @return un booléen vrai si les deux trous sont identiques sinon faux
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hole hole = (Hole) o;
        return id == hole.id &&
                num == hole.num &&
                par == hole.par &&
                walkTime == hole.walkTime &&
                Objects.equals(name, hole.name);
    }

    /**
     * Génere un hashcode pour une instance
     * (Si deux instances sont égaux elles ont en principe le même hashcode)
     * @return un entier généré par un algorithme de hachage
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, name, num, par, walkTime);
    }

    /**
     * Méthode qui permet d'afficher un trou en chaine
     * @return une chaine qui représente un trou
     */
    @Override
    public String toString() {
        return "Hole{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", num=" + num +
                ", par=" + par +
                ", walkTime=" + walkTime +
                '}';
    }
}
