package com.greta94.golfs.controllers;

import com.greta94.golfs.models.Courses;
import com.greta94.golfs.models.Golf;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

@Controller
public class MainControllers {
    
    @RequestMapping("/")
    public String home() {
        return "index";
    }

    @RequestMapping("/golfs")
    public  String golfs(Model model)
    {
    ArrayList<Golf> lesGolfs=generateGolf();
    model.addAttribute("desGolfs",lesGolfs);
    return "golfs";
    }

    public ArrayList<Golf> generateGolf()
    {
        ArrayList<Golf> lesGolf= new ArrayList<>();
        //Les Golfs
        lesGolf.add(new Golf(1,"Château de Cély",new ArrayList<Courses>()));
        lesGolf.add(new Golf(2,"Coudray Montceaux",new ArrayList<Courses>()));
        lesGolf.add(new Golf(3,"Etiolles",new ArrayList<Courses>()));
        lesGolf.add(new Golf(4,"Ozoir",new ArrayList<Courses>()));
        lesGolf.add(new Golf(5,"Courson Stade Français",new ArrayList<Courses>()));
        //Les parcours
        lesGolf.get(0).getCourses().add(new Courses(1, "Sans nom"));
        lesGolf.get(1).getCourses().add(new Courses(2,"Les marroniers"));
        lesGolf.get(2).getCourses().add(new Courses(3,"Les cerfs"));
        lesGolf.get(3).getCourses().add(new Courses(4,"Parcours du château"));
        lesGolf.get(4).getCourses().add(new Courses(5, "Vert-Noir"));
        lesGolf.get(4).getCourses().add(new Courses(6, "Vert-Orange"));
        lesGolf.get(4).getCourses().add(new Courses(7, "Vert-Lilas"));
        lesGolf.get(4).getCourses().add(new Courses(8, "Noir-Orange"));
        lesGolf.get(4).getCourses().add(new Courses(9, "Noir-Lilas"));
        lesGolf.get(4).getCourses().add(new Courses(10, "Noir-Vert"));
        lesGolf.get(4).getCourses().add(new Courses(11, "Lilas-Noir"));
        lesGolf.get(4).getCourses().add(new Courses(12, "Lilas-Vert"));
        lesGolf.get(4).getCourses().add(new Courses(13, "Lilas-Orange"));
        lesGolf.get(4).getCourses().add(new Courses(14, "Orange-Noir"));
        lesGolf.get(4).getCourses().add(new Courses(15, "Orange-Lilas"));
        return lesGolf;
    }


}
